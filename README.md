## Introduction
This project follows the official [getting started](https://docs.djangoproject.com/en/3.0/intro/) tutorial of Django.

The server can be started with `docker-compose up`

## Making model changes
Three steps to make models changes (from [here](https://docs.djangoproject.com/en/3.0/intro/tutorial02/#activating-models))

1. Change your models (in `models.py`).
2. Run `python manage.py makemigrations` to create migrations for those changes
3. Run `python manage.py migrate` to apply those changes to the database.

The SQL statements for the migration can be inspected using `python manage.py sqlmigrate <app> <migration_number>`

## Testing and Coverage
Run the tests with **coverage**:
* `docker-compose exec web coverage run --omit='mysite/*' -p --source='.' manage.py test polls`
  * `-p` for partial report
  * `--omit` for ignoring **mysite** as it's just for configuration
  * Source root set to project root

Combine the tests and generate a report
* `docker-compose exec web coverage combine`
* `docker-compose exec web coverage html`